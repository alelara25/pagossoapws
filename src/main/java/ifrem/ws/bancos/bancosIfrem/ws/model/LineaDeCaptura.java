package ifrem.ws.bancos.bancosIfrem.ws.model;

public class LineaDeCaptura {

    String mensaje;
    String codigo;
    String referencia;
    String descriptiveLegends;

    public LineaDeCaptura ( ) {}

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescriptiveLegends() {
        return descriptiveLegends;
    }

    public void setDescriptiveLegends(String descriptiveLegends) {
        this.descriptiveLegends = descriptiveLegends;
    }
}
