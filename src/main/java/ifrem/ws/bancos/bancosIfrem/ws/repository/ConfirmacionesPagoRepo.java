package ifrem.ws.bancos.bancosIfrem.ws.repository;


import ifrem.ws.bancos.bancosIfrem.ws.model.ConfirmacionesPago;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Repository("confirmacionesPagoJpaRepositoty")
public interface ConfirmacionesPagoRepo extends CrudRepository<ConfirmacionesPago, Serializable> {

    public abstract ConfirmacionesPago findByReferencia(String referencia);

    //@Query("SELECT  new mx.gob.ifrem.Micro.de.Pagos.model.auxiliar.RecaudacionBnaco( t.centroPago,count(t.centroPago) as cantidad, sum(t.importe) as recaudado) FROM ConfirmacionesPago t WHERE t.fechaPago between :fechaInicio  and :fechaFinal and t.centroPago not in ('0') group by t.centroPago order by  t.centroPago")
    //List<RecaudacionBnaco> getRecaudacionBancos(@Param("fechaInicio") Date fechaInicio, @Param("fechaFinal") Date fechaFinal);
}