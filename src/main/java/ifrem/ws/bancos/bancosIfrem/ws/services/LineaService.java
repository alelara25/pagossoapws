package ifrem.ws.bancos.bancosIfrem.ws.services;

import com.ifrem.services_soap.*;
import ifrem.ws.bancos.bancosIfrem.ws.model.ConfirmacionesPago;
import ifrem.ws.bancos.bancosIfrem.ws.model.FormatoUniversal;
import ifrem.ws.bancos.bancosIfrem.ws.repository.ConfirmacionesPagoRepo;
import ifrem.ws.bancos.bancosIfrem.ws.repository.FormatoUniversalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class LineaService {


    @Autowired
    @Qualifier("FormatoUniversalServiceImpl")
    private FormatoUniversalService formatoUniversalService;

    @Autowired
    private ConfirmacionesPagoRepo confirmacionesPagoRepo;

    @Autowired
    private FormatoUniversalRepo formatoUniversalRepo;

    private static final Map<String, Referencia> referencias = new HashMap<>();

    @PostConstruct
    public void initialize(){

        Referencia linea1 = new Referencia();
        Referencia linea2 = new Referencia();

        linea1.setCodigo("1000");
        linea1.setMensaje("prueba");
        linea1.setDescriptiveLegends("linea");
        linea1.setReferencia("111111111");

        linea2.setCodigo("1000");
        linea2.setMensaje("prueba");
        linea2.setDescriptiveLegends("linea");
        linea2.setReferencia("22222222");


        referencias.put(linea1.getReferencia(), linea1);
        referencias.put(linea2.getReferencia(), linea2);

    }

    public Referencia getReferencias(String linea){
        System.out.println("buscarLinea: " + linea);
        Referencia linea_ = new Referencia();
        FormatoUniversal formatoUniversal = new FormatoUniversal();
        formatoUniversal = formatoUniversalService.findById(linea);

        linea_.setReferencia(linea);
        linea_.setDescriptiveLegends(linea);

        if(formatoUniversal != null){
            if(formatoUniversal.getStatus()==1){
                linea_.setMensaje("IMPORTE DE PAGO");
                linea_.setCodigo(String.valueOf(formatoUniversal.getTotal()));
            }else{
                linea_.setMensaje("Linea de Captura YA PAGADA");
                linea_.setCodigo("0");
            }

        }else {
            linea_.setMensaje("Linea NO VALIDA/INEXISTENTE");
            linea_.setCodigo("1010");
        }

        //return referencias.get(linea);

        return linea_;
    }

    public Pago getPago(String linea, String banco, String importe){

        System.out.println("entrando a cierre de operacion");
        System.out.println("entrando a cierre de operacion : " +  linea + " banco: " + banco + " importe: " + importe);
        Pago pago = new Pago();
        FormatoUniversal formatoUniversal = new FormatoUniversal();
        formatoUniversal = formatoUniversalService.findById(linea);

        if(formatoUniversal != null){

            if(formatoUniversal.getStatus()==1){

                    if(Integer.parseInt(importe)>0){
                        try{

                            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

                            SimpleDateFormat formatoFechaHoy = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                            String hoy_ =  formatoFecha.format(new Date());

                            System.out.println("FECHA INGREWSO::::::: " + hoy_ + " 00:00");

                            System.out.println("procede el pago de la linea: " + linea);
                            ConfirmacionesPago confirmacionesPago = new ConfirmacionesPago();
                            confirmacionesPago.setCentroPago(Integer.parseInt(banco));
                            confirmacionesPago.setFechaIngreso(formatoFechaHoy.parse(hoy_ + " 00:00"));
                            confirmacionesPago.setFechaPago(formatoFechaHoy.parse(hoy_ + " 00:00"));
                            confirmacionesPago.setReferencia(linea);
                            confirmacionesPago.setImporte(Integer.parseInt(importe));
                            confirmacionesPago.setDetalles("PAGO CONFIRMADO MEDIANTE WEB SERVICES - CLOUD 2.0");

                            confirmacionesPagoRepo.save(confirmacionesPago);

                            formatoUniversal.setStatus(2);
                            formatoUniversalRepo.save(formatoUniversal);

                            pago.setMensaje("PAGO REALIZADO CORRECTAMENTE - Linea de Captura " + linea + " MONTO: " + importe);
                            pago.setCodigo("1000");


                        }catch (Exception e){
                            System.out.println("ERRORES DE PAGO INSERT: " + e);
                            pago.setMensaje("ERROR DE COMUNICACION");
                            pago.setCodigo("1001");
                        }

                    }else{
                        pago.setMensaje("El importe debe ser mayor a cero");
                        pago.setCodigo("1004");
                    }

            }else{
                pago.setMensaje("Linea de Captura YA PAGADA");
                pago.setCodigo("1002");
            }
        }else {
            pago.setMensaje("Linea NO VALIDA/INEXISTENTE");
            pago.setCodigo("1003");
        }

        return pago;
    }



    /* -----  adeudo */

    public Adeudo adeudo(String linea){
        System.out.println("buscarLinea: " + linea);

        Adeudo linea_ = new Adeudo();
        FormatoUniversal formatoUniversal = new FormatoUniversal();
        formatoUniversal = formatoUniversalService.findById(linea);


        if(formatoUniversal != null){
            if(formatoUniversal.getStatus()==1){
                linea_.setMensaje("IMPORTE DE PAGO");
                linea_.setCodigo(String.valueOf(formatoUniversal.getTotal()));
            }else{
                linea_.setMensaje("Linea de Captura YA PAGADA");
                linea_.setCodigo("0");
            }

        }else {
            linea_.setMensaje("Linea NO VALIDA/INEXISTENTE");
            linea_.setCodigo("1010");
        }

        //return referencias.get(linea);

        return linea_;
    }

    /* -----  tipo */

    public TipoDeLinea tipoDeLinea(String linea){
        System.out.println("buscarLinea: " + linea);

        TipoDeLinea linea_ = new TipoDeLinea();


        FormatoUniversal formatoUniversal = new FormatoUniversal();
        formatoUniversal = formatoUniversalService.findById(linea);


        if(formatoUniversal != null){
            if(linea.contains("123I")){
                linea_.setTipoDeLinea("COMERCIO");
            }else{
                linea_.setTipoDeLinea("PROPIEDAD");
            }

        }else{
            linea_.setTipoDeLinea("1001@Linea NO VALIDA/INEXISTENTE");
        }


        System.out.println("TIPO_De_LINEA_ " + linea + " --- " + linea_.getTipoDeLinea());

        return linea_;
    }




    /* -----  consultaReferencia */

    public ConsultaReferencia consultaReferencia(String linea){
        System.out.println("buscarLinea: " + linea);

        ConsultaReferencia consultaReferencia = new ConsultaReferencia();


        FormatoUniversal formatoUniversal = new FormatoUniversal();
        formatoUniversal = formatoUniversalService.findById(linea);


        if(formatoUniversal != null){
                consultaReferencia.setConsultaReferencia("Línea Valida");
        }else{
            consultaReferencia.setConsultaReferencia("Linea NO VALIDA/INEXISTENTE");
        }

        System.out.println("consultaReferencia - buscarLinea: " + linea + " -- " + consultaReferencia.getConsultaReferencia());


        return consultaReferencia;
    }



    /* -----  cierreOperacion */

    public CierreOperacion cierreOperacion(String linea, String banco, String importe){

        System.out.println("entrando a cierre de operacion");
        System.out.println("entrando a cierre de operacion : " +  linea + " banco: " + banco + " importe: " + importe);
        CierreOperacion pago = new CierreOperacion();
        FormatoUniversal formatoUniversal = new FormatoUniversal();
        formatoUniversal = formatoUniversalService.findById(linea);

        if(formatoUniversal != null){

            if(formatoUniversal.getStatus()==1){

                if(Integer.parseInt(importe)>0){
                    try{

                        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

                        SimpleDateFormat formatoFechaHoy = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                        String hoy_ =  formatoFecha.format(new Date());

                        System.out.println("FECHA INGREWSO::::::: " + hoy_ + " 00:00");

                        System.out.println("procede el pago de la linea: " + linea);
                        ConfirmacionesPago confirmacionesPago = new ConfirmacionesPago();
                        confirmacionesPago.setCentroPago(Integer.parseInt(banco));
                        confirmacionesPago.setFechaIngreso(formatoFechaHoy.parse(hoy_ + " 00:00"));
                        confirmacionesPago.setFechaPago(formatoFechaHoy.parse(hoy_ + " 00:00"));
                        confirmacionesPago.setReferencia(linea);
                        confirmacionesPago.setImporte(Integer.parseInt(importe));
                        confirmacionesPago.setDetalles("PAGO CONFIRMADO MEDIANTE WEB SERVICES - CLOUD 2.0");

                        confirmacionesPagoRepo.save(confirmacionesPago);

                        formatoUniversal.setStatus(2);
                        formatoUniversalRepo.save(formatoUniversal);

                        pago.setMensaje("PAGO REALIZADO CORRECTAMENTE - Linea de Captura " + linea + " MONTO: " + importe);
                        pago.setCodigo("1000");


                    }catch (Exception e){
                        System.out.println("ERRORES DE PAGO INSERT: " + e);
                        pago.setMensaje("ERROR DE COMUNICACION");
                        pago.setCodigo("1005");
                    }

                }else{
                    pago.setMensaje("El importe debe ser mayor a cero");
                    pago.setCodigo("1003");
                }

            }else{
                pago.setMensaje("Linea de Captura YA PAGADA");
                pago.setCodigo("1002");
            }
        }else {
            pago.setMensaje("Linea NO VALIDA/INEXISTENTE");
            pago.setCodigo("1001");
        }

        return pago;
    }

}
