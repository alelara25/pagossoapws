package ifrem.ws.bancos.bancosIfrem.ws.model;

import java.util.Date;
import javax.persistence.*;


@Entity
@Table(name="confirmacionespago")
public class ConfirmacionesPago {

    @Id
    @GeneratedValue(generator = "seq_confirmacionespago")
    @SequenceGenerator(name = "seq_confirmacionespago", sequenceName = "seq_confirmacionespago", allocationSize = 1)
    @Column(name = "idconfirmacion")
    private Long id;

    @Column(name = "referencia")
    private String referencia;

    @Column(name = "fechapago")
    private Date fechaPago;

    @Column(name = "importepagado")
    private int importe;

    @Column(name = "idcentropago")
    private int centroPago;

    @Column(name = "fechaingreso")
    private Date fechaIngreso;

   @Column(name = "detalles")
    private String detalles;


   /* private String bancoNombre;

    public String getBancoNombre() {
        return bancoNombre;
    }

    public void setBancoNombre(String bancoNombre) {
        this.bancoNombre = bancoNombre;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public int getImporte() {
        return importe;
    }

    public void setImporte(int importe) {
        this.importe = importe;
    }

    public int getCentroPago() {
        return centroPago;
    }

    public void setCentroPago(int centroPago) {
        this.centroPago = centroPago;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
}