package ifrem.ws.bancos.bancosIfrem.ws.services;

import ifrem.ws.bancos.bancosIfrem.ws.model.FormatoUniversal;
import org.springframework.stereotype.Service;



public interface FormatoUniversalService {
    public abstract FormatoUniversal findById(String referencia);
    public FormatoUniversal guardarCaso(FormatoUniversal casos);
}