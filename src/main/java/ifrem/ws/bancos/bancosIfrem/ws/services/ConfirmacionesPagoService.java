package ifrem.ws.bancos.bancosIfrem.ws.services;

import ifrem.ws.bancos.bancosIfrem.ws.model.ConfirmacionesPago;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ConfirmacionesPagoService {
    public abstract ConfirmacionesPago findByReferencia(String referencia);
    //List<RecaudacionBnaco> getRecaudacionBancos( Date fechaInicio, Date fechaFinal);
}