package ifrem.ws.bancos.bancosIfrem.ws.repository;

import ifrem.ws.bancos.bancosIfrem.ws.model.FormatoUniversal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("formatoUniversalJpaRepositoty")
public interface FormatoUniversalRepo extends CrudRepository<FormatoUniversal, Serializable> {

    public abstract FormatoUniversal findById(String referencia);

}