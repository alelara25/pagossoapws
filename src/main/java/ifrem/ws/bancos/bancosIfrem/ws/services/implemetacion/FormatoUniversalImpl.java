package ifrem.ws.bancos.bancosIfrem.ws.services.implemetacion;
import ifrem.ws.bancos.bancosIfrem.ws.model.FormatoUniversal;
import ifrem.ws.bancos.bancosIfrem.ws.repository.FormatoUniversalRepo;
import ifrem.ws.bancos.bancosIfrem.ws.services.FormatoUniversalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("FormatoUniversalServiceImpl")
public class FormatoUniversalImpl implements FormatoUniversalService {

    @Autowired
    @Qualifier("formatoUniversalJpaRepositoty")
    private FormatoUniversalRepo formatoUniversalRepo;


    @Override
    public FormatoUniversal findById(String referencia) {
        return formatoUniversalRepo.findById(referencia);
    }


    @Override
    public FormatoUniversal guardarCaso(FormatoUniversal casos) {
        return formatoUniversalRepo.save(casos);
    }
}