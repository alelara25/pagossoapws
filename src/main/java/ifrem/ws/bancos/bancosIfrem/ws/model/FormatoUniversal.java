package ifrem.ws.bancos.bancosIfrem.ws.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="formatosuniversales")
public class FormatoUniversal {

    @Id
    @GeneratedValue
    @Column(name = "referencia")
    private String id;

    @Column(name = "idcontribuyente")
    private Long contribuyente;

    //@JoinColumn(name="IDCONTRIBUYENTE", unique = true)
    //@OneToOne(cascade = CascadeType.ALL)
    //Contribuyente contribuyente;

    @Column(name = "fechaemision")
    private Date fechaEmision;

    @Column(name = "fechalimite")
    private Date fechaLimite;

    @Column(name = "total")
    private int total;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /*public Contribuyente getContribuyente() {
        return contribuyente;
    }

    public void setContribuyente(Contribuyente contribuyente) {
        this.contribuyente = contribuyente;
    }*/

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    @Column(name = "idstatus")
    private int status;

    @Column(name = "rec")
    private String rec;



}