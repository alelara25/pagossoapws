package ifrem.ws.bancos.bancosIfrem.ws.services.implemetacion;

import ifrem.ws.bancos.bancosIfrem.ws.model.ConfirmacionesPago;
import ifrem.ws.bancos.bancosIfrem.ws.repository.ConfirmacionesPagoRepo;
import ifrem.ws.bancos.bancosIfrem.ws.services.ConfirmacionesPagoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("ConfirmacionesPagoServiceImpl")
public class ConfirmacionesPagoImpl implements ConfirmacionesPagoService {


    @Autowired
    @Qualifier("confirmacionesPagoJpaRepositoty")
    private ConfirmacionesPagoRepo confirmacionesPagoRepo;

    @Override
    public ConfirmacionesPago findByReferencia(String referencia) {
        return confirmacionesPagoRepo.findByReferencia(referencia);
    }


}