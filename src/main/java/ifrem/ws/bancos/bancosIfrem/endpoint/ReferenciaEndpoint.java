package ifrem.ws.bancos.bancosIfrem.endpoint;

import com.ifrem.services_soap.*;
import ifrem.ws.bancos.bancosIfrem.ws.services.LineaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class ReferenciaEndpoint {


    @Autowired
    private LineaService lineaService;

    @PayloadRoot(namespace = "http://ifrem.com/services-soap", localPart = "getLineaRequest")
    @ResponsePayload
    public GetLineaResponse getLineaRequest(@RequestPayload GetLineaRequest request){
        GetLineaResponse getLineaResponse = new GetLineaResponse();
        getLineaResponse.setReferencia(lineaService.getReferencias(request.getLinea()));
        return getLineaResponse;
    }

    @PayloadRoot(namespace = "http://ifrem.com/services-soap", localPart = "getcierreOperacionRequest")
    @ResponsePayload
    public GetcierreOperacionResponse getcierreOperacionRequest(@RequestPayload GetcierreOperacionRequest request){
        GetcierreOperacionResponse getLineaResponse = new GetcierreOperacionResponse();
        getLineaResponse.setReferenciaPago(lineaService.getPago(request.getLinea(), request.getBanco(), request.getMonto()));
        return getLineaResponse;
    }

    @PayloadRoot(namespace = "http://ifrem.com/services-soap", localPart = "adeudoRequest")
    @ResponsePayload
    public AdeudoResponse AdeudoRequest(@RequestPayload AdeudoRequest request){
        GetcierreOperacionResponse getLineaResponse = new GetcierreOperacionResponse();
        AdeudoResponse adeudo_ = new AdeudoResponse();
        //getLineaResponse.setReferenciaPago(lineaService.getPago(request.getReferencia()));
        adeudo_.setAdeudo(lineaService.adeudo(request.getReferencia()));
        return adeudo_;
    }

    @PayloadRoot(namespace = "http://ifrem.com/services-soap", localPart = "tipoDeLineaRequest")
    @ResponsePayload
    public TipoDeLineaResponse TipoDeLineaRequest(@RequestPayload TipoDeLineaRequest request){
        TipoDeLineaResponse tipo = new TipoDeLineaResponse();
        tipo.setTipoDeLinea(lineaService.tipoDeLinea(request.getReferencia()));
        return tipo;
    }

    @PayloadRoot(namespace = "http://ifrem.com/services-soap", localPart = "consultaReferenciaRequest")
    @ResponsePayload
    public ConsultaReferenciaResponse consultaReferenciaRequest(@RequestPayload ConsultaReferenciaRequest request){
        ConsultaReferenciaResponse tipo = new ConsultaReferenciaResponse();
        tipo.setConsultaReferencia(lineaService.consultaReferencia(request.getReferencia()));
        return tipo;
    }

    @PayloadRoot(namespace = "http://ifrem.com/services-soap", localPart = "cierreOperacionRequest")
    @ResponsePayload
    public CierreOperacionResponse cierreOperacionRequest(@RequestPayload CierreOperacionRequest request){
        CierreOperacionResponse tipo = new CierreOperacionResponse();
        tipo.setCierreOperacion(lineaService.cierreOperacion(request.getReferencia(), request.getBanco(), request.getMonto()));
        return tipo;
    }




}
