//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.03.21 a las 01:06:39 PM CST 
//


package com.ifrem.services_soap;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ifrem.services_soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ifrem.services_soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetLineaRequest }
     * 
     */
    public GetLineaRequest createGetLineaRequest() {
        return new GetLineaRequest();
    }

    /**
     * Create an instance of {@link CierreOperacionRequest }
     * 
     */
    public CierreOperacionRequest createCierreOperacionRequest() {
        return new CierreOperacionRequest();
    }

    /**
     * Create an instance of {@link GetcierreOperacionResponse }
     * 
     */
    public GetcierreOperacionResponse createGetcierreOperacionResponse() {
        return new GetcierreOperacionResponse();
    }

    /**
     * Create an instance of {@link Pago }
     * 
     */
    public Pago createPago() {
        return new Pago();
    }

    /**
     * Create an instance of {@link ConsultaReferenciaRequest }
     * 
     */
    public ConsultaReferenciaRequest createConsultaReferenciaRequest() {
        return new ConsultaReferenciaRequest();
    }

    /**
     * Create an instance of {@link GetLineaResponse }
     * 
     */
    public GetLineaResponse createGetLineaResponse() {
        return new GetLineaResponse();
    }

    /**
     * Create an instance of {@link Referencia }
     * 
     */
    public Referencia createReferencia() {
        return new Referencia();
    }

    /**
     * Create an instance of {@link TipoDeLineaResponse }
     * 
     */
    public TipoDeLineaResponse createTipoDeLineaResponse() {
        return new TipoDeLineaResponse();
    }

    /**
     * Create an instance of {@link TipoDeLinea }
     * 
     */
    public TipoDeLinea createTipoDeLinea() {
        return new TipoDeLinea();
    }

    /**
     * Create an instance of {@link ConsultaReferenciaResponse }
     * 
     */
    public ConsultaReferenciaResponse createConsultaReferenciaResponse() {
        return new ConsultaReferenciaResponse();
    }

    /**
     * Create an instance of {@link ConsultaReferencia }
     * 
     */
    public ConsultaReferencia createConsultaReferencia() {
        return new ConsultaReferencia();
    }

    /**
     * Create an instance of {@link AdeudoRequest }
     * 
     */
    public AdeudoRequest createAdeudoRequest() {
        return new AdeudoRequest();
    }

    /**
     * Create an instance of {@link CierreOperacionResponse }
     * 
     */
    public CierreOperacionResponse createCierreOperacionResponse() {
        return new CierreOperacionResponse();
    }

    /**
     * Create an instance of {@link CierreOperacion }
     * 
     */
    public CierreOperacion createCierreOperacion() {
        return new CierreOperacion();
    }

    /**
     * Create an instance of {@link GetcierreOperacionRequest }
     * 
     */
    public GetcierreOperacionRequest createGetcierreOperacionRequest() {
        return new GetcierreOperacionRequest();
    }

    /**
     * Create an instance of {@link AdeudoResponse }
     * 
     */
    public AdeudoResponse createAdeudoResponse() {
        return new AdeudoResponse();
    }

    /**
     * Create an instance of {@link Adeudo }
     * 
     */
    public Adeudo createAdeudo() {
        return new Adeudo();
    }

    /**
     * Create an instance of {@link TipoDeLineaRequest }
     * 
     */
    public TipoDeLineaRequest createTipoDeLineaRequest() {
        return new TipoDeLineaRequest();
    }

}
