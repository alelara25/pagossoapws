//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.03.21 a las 01:06:39 PM CST 
//


package com.ifrem.services_soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipoDeLinea" type="{http://ifrem.com/services-soap}tipoDeLinea"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tipoDeLinea"
})
@XmlRootElement(name = "tipoDeLineaResponse")
public class TipoDeLineaResponse {

    @XmlElement(required = true)
    protected TipoDeLinea tipoDeLinea;

    /**
     * Obtiene el valor de la propiedad tipoDeLinea.
     * 
     * @return
     *     possible object is
     *     {@link TipoDeLinea }
     *     
     */
    public TipoDeLinea getTipoDeLinea() {
        return tipoDeLinea;
    }

    /**
     * Define el valor de la propiedad tipoDeLinea.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDeLinea }
     *     
     */
    public void setTipoDeLinea(TipoDeLinea value) {
        this.tipoDeLinea = value;
    }

}
